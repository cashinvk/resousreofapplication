package com.example.cashinvk.resourceofapplication;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((TextView) findViewById(R.id.textInfo)).setText("Строковый ресурс: " + getString(R.string.myText));
        ((TextView) findViewById(R.id.colorInfo)).setText("Цветовой ресурс: " + getString(R.color.colorText));
        ((TextView) findViewById(R.id.sizeInfo)).setText("Ресурс размера(px): " + getResources().getDimension(R.dimen.textSizeView));
    }
}
